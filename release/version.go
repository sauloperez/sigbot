package release

import (
	"fmt"
	"github.com/blang/semver"
	"strings"
)

type Version struct {
	number string
}

func NewVersion(number string) *Version {
	return &Version{number}
}

// TODO: add prefix string in struct so NextMinor becomes simpler. No need for
// conditionals; simply prepend the computed nextVersion with the prefix.
func (v *Version) NextMinor() string {
	number := strings.TrimPrefix(v.number, "v")

	version, _ := semver.Make(number)
	nextNumber := fmt.Sprintf("%d.%d.%d", version.Major, version.Minor+1, 0)

	nextVersion := ""
	if number == v.number {
		nextVersion = fmt.Sprintf("%v", nextNumber)
	} else {
		nextVersion = fmt.Sprintf("v%v", nextNumber)
	}

	return nextVersion
}
