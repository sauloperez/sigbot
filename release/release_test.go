package release

import (
	"context"
	"errors"
	"github.com/google/go-github/github"
	"reflect"
	"testing"
	"time"
)

// Implementation of the GithubClient as a fake object
type FakeClient struct{}

func (f *FakeClient) DraftRelease(r Release) (Release, error) {
	r.URL = "releaseURL"

	return r, nil
}

func (f *FakeClient) GetLatestRelease() (Release, error) {
	publisedAt := github.Timestamp{time.Date(2002, time.February, 10, 15, 30, 0, 0, time.UTC)}

	return Release{TagName: "v1.1.1", PublishedAt: publisedAt}, nil
}

func (f *FakeClient) GetPullRequestsSince(date time.Time) ([]PullRequest, error) {
	pullRequests := []PullRequest{
		PullRequest{2345, "https://github.com/openfoodfoundation/openfoodnetwork/pull/2345", "2345's title"},
		PullRequest{9083, "https://github.com/openfoodfoundation/openfoodnetwork/pull/9083", "9083's title"},
	}

	return pullRequests, nil
}

func TestCreateRelease(t *testing.T) {
	expected := Release{
		URL: "releaseURL",
		Description: `## Changelog
[#2345](https://github.com/openfoodfoundation/openfoodnetwork/pull/2345) 2345's title
[#9083](https://github.com/openfoodfoundation/openfoodnetwork/pull/9083) 9083's title
`,
		TagName:    "v1.2.0",
		CodeName:   "Foo",
		Committish: "4126c2",
	}

	service := GithubReleaseService{&FakeClient{}}
	r := Release{CodeName: "Foo", Committish: "4126c2"}

	actual, _ := service.CreateRelease(r)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

func TestCreateReleaseWithoutCommittish(t *testing.T) {
	expected := Release{
		URL: "releaseURL",
		Description: `## Changelog
[#2345](https://github.com/openfoodfoundation/openfoodnetwork/pull/2345) 2345's title
[#9083](https://github.com/openfoodfoundation/openfoodnetwork/pull/9083) 9083's title
`,
		TagName:    "v1.2.0",
		Committish: "e0609e3",
	}

	service := GithubReleaseService{&FakeClient{}}
	r := Release{}

	actual, _ := service.CreateRelease(r)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

// Implementation of the GithubClient as a fake object whose GetPullRequestsSince returns an empty list
type EmptyPRsClient struct{}

func (f *EmptyPRsClient) DraftRelease(r Release) (Release, error) {
	r.URL = "releaseURL"

	return r, nil
}

func (f *EmptyPRsClient) GetLatestRelease() (Release, error) {
	publisedAt := github.Timestamp{time.Date(2002, time.February, 10, 15, 30, 0, 0, time.UTC)}

	return Release{TagName: "v1.1.1", PublishedAt: publisedAt}, nil
}

func (f *EmptyPRsClient) GetPullRequestsSince(date time.Time) ([]PullRequest, error) {
	pullRequests := []PullRequest{}

	return pullRequests, nil
}

func TestCreateReleaseWithoutPRs(t *testing.T) {
	expected := "There are no merged PRs since last release!"

	service := GithubReleaseService{&EmptyPRsClient{}}
	r := Release{}

	_, err := service.CreateRelease(r)
	actual := err.Error()

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

// Implementation of the GithubClient as a test spy
type spyApiClient struct {
	actualOwner             string
	actualRepo              string
	actualRepositoryRelease *github.RepositoryRelease
}

func (f *spyApiClient) CreateRelease(ctx context.Context, owner, repo string, release *github.RepositoryRelease) (*github.RepositoryRelease, *github.Response, error) {
	f.actualOwner = owner
	f.actualRepo = repo
	f.actualRepositoryRelease = release

	url := "releaseURL"
	body := "body"
	tag := "v1.1.1"

	return &github.RepositoryRelease{
		HTMLURL: &url,
		Body:    &body,
		TagName: &tag,
	}, nil, nil
}

func (f *spyApiClient) GetLatestRelease(ctx context.Context, owner string, repo string) (*github.RepositoryRelease, *github.Response, error) {
	return nil, nil, nil
}

func (f *spyApiClient) Issues(ctx context.Context, query string, opt *github.SearchOptions) (*github.IssuesSearchResult, *github.Response, error) {
	return &github.IssuesSearchResult{}, &github.Response{}, nil
}

func TestCreateReleaseArguments(t *testing.T) {
	expectedOwner := "openfoodfoundation"
	expectedRepo := "openfoodnetwork"

	expectedName := "v0 Foo"
	expectedTagName := "v0"
	expectedDraft := true
	expectedBody := "description"

	expectedRepositoryRelease := &github.RepositoryRelease{
		Name:    &expectedName,
		Draft:   &expectedDraft,
		TagName: &expectedTagName,
		Body:    &expectedBody,
	}

	APIClient := &spyApiClient{}
	githubClient := GithubClient{
		owner:     "openfoodfoundation",
		repo:      "openfoodnetwork",
		APIClient: APIClient,
	}

	r := Release{
		CodeName:    "Foo",
		TagName:     "v0",
		Description: "description",
	}

	githubClient.DraftRelease(r)

	if APIClient.actualOwner != expectedOwner {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expectedOwner, APIClient.actualOwner)
	}
	if APIClient.actualRepo != expectedRepo {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expectedRepo, APIClient.actualRepo)
	}
	if !reflect.DeepEqual(APIClient.actualRepositoryRelease, expectedRepositoryRelease) {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expectedRepositoryRelease, APIClient.actualRepositoryRelease)
	}
}

func TestCreateReleaseInfersNextVersion(t *testing.T) {
	expected := "v1.2.0"

	service := GithubReleaseService{&FakeClient{}}
	r := Release{CodeName: "Foo"}

	release, _ := service.CreateRelease(r)

	actual := release.TagName

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

func TestCreateReleaseWithVersion(t *testing.T) {
	expected := "v0"

	service := GithubReleaseService{&FakeClient{}}
	r := Release{CodeName: "Foo", TagName: "v0"}

	release, _ := service.CreateRelease(r)

	actual := release.TagName

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

// Implementation of the GithubClient that always throws an error
type ErroredClient struct{}

func (f *ErroredClient) DraftRelease(r Release) (Release, error) {
	return Release{}, nil
}

func (f *ErroredClient) GetLatestRelease() (Release, error) {
	return Release{}, errors.New("GetLatestRelease error")
}

func (g *ErroredClient) GetPullRequestsSince(date time.Time) ([]PullRequest, error) {
	return nil, nil
}

func TestCreateReleaseFailsEarly(t *testing.T) {
	expected := "GetLatestRelease error"

	service := GithubReleaseService{&ErroredClient{}}
	r := Release{CodeName: "Foo"}
	_, err := service.CreateRelease(r)
	actual := err.Error()

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}
