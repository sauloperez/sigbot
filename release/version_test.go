package release

import "testing"

func TestNextMinorWithoutV(t *testing.T) {
	expected := "1.1.0"

	version := NewVersion("1.0.0")
	actual := version.NextMinor()

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

func TestNextMinorWithV(t *testing.T) {
	expected := "v1.30.0"

	version := NewVersion("v1.29.2")
	actual := version.NextMinor()

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}
