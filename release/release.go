package release

import (
	"errors"
	"fmt"
	"github.com/google/go-github/github"
	"os"
	"time"
)

type Release struct {
	URL         string
	TagName     string
	CodeName    string
	Description string
	PublishedAt github.Timestamp
	Committish  string
}

type ReleaseService interface {
	CreateRelease(Release) Release
}

type RepositoryClient interface {
	DraftRelease(Release) (Release, error)
	GetLatestRelease() (Release, error)
	GetPullRequestsSince(time.Time) ([]PullRequest, error)
}

type GithubReleaseService struct {
	Client RepositoryClient
}

func NewGithubReleaseService() *GithubReleaseService {
	owner := os.Getenv("SIGBOT_OWNER")
	repo := os.Getenv("SIGBOT_REPO")

	if owner == "" {
		owner = "openfoodfoundation"
	}
	if repo == "" {
		repo = "openfoodnetwork"
	}

	githubClient := NewGithubClient(owner, repo)

	return &GithubReleaseService{Client: githubClient}
}

// TODO: I think r should be a pointer to a Release and then we could complete
// the empty fields with the data returned by Github. I don't don't what
// benefits it'd bring other than less memory usage.
func (g *GithubReleaseService) CreateRelease(r Release) (Release, error) {
	latestRelease, err := g.Client.GetLatestRelease()

	if err != nil {
		return r, err
	}

	if r.TagName == "" {
		v := NewVersion(latestRelease.TagName)
		r.TagName = v.NextMinor()
	}

	date := latestRelease.PublishedAt.String()
	t, err := time.Parse("2006-01-02 15:04:05 -0700 MST", date)

	if err != nil {
		fmt.Println(err)
	}

	prs, _ := g.Client.GetPullRequestsSince(t)

	if len(prs) == 0 {
		return Release{}, errors.New("There are no merged PRs since last release!")
	}

	r.Description = "## Changelog\n"
	r.Description += g.toDescription(prs)

	// TODO: pick the last commit in master if no committish is provided.
	// This way upcoming PRs can be merged without enlarging the draft
	// release.
	if r.Committish == "" {
		r.Committish = "e0609e3"
	}

	release, err := g.Client.DraftRelease(r)

	return release, err
}

func (g *GithubReleaseService) toDescription(prs []PullRequest) string {
	var description string

	for _, pr := range prs {
		description += fmt.Sprintf("[#%v](%s) %s\n", pr.Number, pr.URL, pr.Title)
	}

	return description
}
