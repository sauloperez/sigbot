package release

import (
	"context"
	"errors"
	"github.com/google/go-github/github"
	"reflect"
	"testing"
	"time"
)

// Implements GithubAPIClient as a fake object to simulate what Github's API
// would return.
type FakeAPIClient struct{}

// This method acts as Github's servers themselves. Whatever we return here is
// what a Github response would contain.
func (f FakeAPIClient) CreateRelease(ctx context.Context, owner string, repo string, release *github.RepositoryRelease) (*github.RepositoryRelease, *github.Response, error) {
	url := "releaseURL"
	body := "description"

	release.HTMLURL = &url
	release.Body = &body

	return release, nil, nil
}

func (f FakeAPIClient) GetLatestRelease(ctx context.Context, owner string, repo string) (*github.RepositoryRelease, *github.Response, error) {
	tagName := "v1.1.1"
	url := "lastReleaseURL"
	body := "last release body"
	publisedAt := github.Timestamp{time.Date(2002, time.February, 10, 15, 30, 0, 0, time.UTC)}
	commitish := "9fcd708"

	return &github.RepositoryRelease{
		TagName:         &tagName,
		HTMLURL:         &url,
		Body:            &body,
		PublishedAt:     &publisedAt,
		TargetCommitish: &commitish,
	}, nil, nil
}

func (f FakeAPIClient) Issues(ctx context.Context, query string, opt *github.SearchOptions) (*github.IssuesSearchResult, *github.Response, error) {
	return &github.IssuesSearchResult{}, &github.Response{}, nil
}

func TestDraftRelease(t *testing.T) {
	expected := Release{
		URL:         "releaseURL",
		TagName:     "Foo",
		Description: "description",
	}

	githubClient := &GithubClient{APIClient: FakeAPIClient{}, owner: "o", repo: "r"}
	r := Release{
		CodeName:    "v1",
		TagName:     "Foo",
		Description: "description",
	}

	actual, _ := githubClient.DraftRelease(r)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

func TestGetLatestRelease(t *testing.T) {
	expected := Release{
		TagName:     "v1.1.1",
		URL:         "lastReleaseURL",
		Description: "last release body",
		Committish:  "9fcd708",
		PublishedAt: github.Timestamp{time.Date(2002, time.February, 10, 15, 30, 0, 0, time.UTC)},
	}

	githubClient := &GithubClient{APIClient: FakeAPIClient{}, owner: "o", repo: "r"}
	actual, _ := githubClient.GetLatestRelease()

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

// Implementation of the SearchAPI as a test spy
type SpySearchAPI struct {
	query string
}

func (s *SpySearchAPI) Issues(ctx context.Context, query string, opt *github.SearchOptions) (*github.IssuesSearchResult, *github.Response, error) {
	total := 2
	incompleteResults := false

	num3280 := 3280
	pr3280 := "https://github.com/openfoodfoundation/openfoodnetwork/pull/3280"
	title3280 := "3280's title"
	num3115 := 3115
	pr3115 := "https://github.com/openfoodfoundation/openfoodnetwork/pull/3115"
	title3115 := "3115's title"

	result := &github.IssuesSearchResult{
		Total:             &total,
		IncompleteResults: &incompleteResults,
		Issues: []github.Issue{
			github.Issue{Number: &num3280, HTMLURL: &pr3280, Title: &title3280},
			github.Issue{Number: &num3115, HTMLURL: &pr3115, Title: &title3115},
		},
	}

	s.query = query

	return result, &github.Response{}, nil
}

func TestGetPullRequestsSince(t *testing.T) {
	expected := make([]PullRequest, 2)
	expected[0] = PullRequest{3280, "https://github.com/openfoodfoundation/openfoodnetwork/pull/3280", "3280's title"}
	expected[1] = PullRequest{3115, "https://github.com/openfoodfoundation/openfoodnetwork/pull/3115", "3115's title"}

	githubClient := &GithubClient{APIClient: FakeAPIClient{}, owner: "o", repo: "r", Search: &SpySearchAPI{}}

	time, _ := time.Parse(time.RFC3339, "2019-05-05T01:14:52Z")
	actual, _ := githubClient.GetPullRequestsSince(time)

	for i := range actual {
		if actual[i] != expected[i] {
			t.Errorf("Test failed, expected: '%v', got: '%v'", expected, actual)
		}
	}
}

func TestGetPullRequestsSinceQuery(t *testing.T) {
	expected := "is:pr repo:o/r merged:>2019-05-05T01:14:52+0000"

	spySearchAPI := &SpySearchAPI{}
	g := &GithubClient{APIClient: FakeAPIClient{}, owner: "o", repo: "r", Search: spySearchAPI}

	time, _ := time.Parse(time.RFC3339, "2019-05-05T01:14:52Z")
	g.GetPullRequestsSince(time)

	actual := spySearchAPI.query

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

// Implementation of the SearchAPI that always throws an error
type ErroredSearchAPI struct{}

func (e *ErroredSearchAPI) Issues(ctx context.Context, query string, opt *github.SearchOptions) (*github.IssuesSearchResult, *github.Response, error) {
	result := &github.IssuesSearchResult{}
	err := errors.New("GetPullRequestsSince error")

	return result, &github.Response{}, err
}

func TestErroredGetPullRequestsSince(t *testing.T) {
	expected := errors.New("GetPullRequestsSince error").Error()

	erroredSearchAPI := &ErroredSearchAPI{}
	g := &GithubClient{APIClient: FakeAPIClient{}, owner: "o", repo: "r", Search: erroredSearchAPI}

	time, _ := time.Parse(time.RFC3339, "2019-05-05T01:14:52Z")
	_, err := g.GetPullRequestsSince(time)

	actual := err.Error()

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}
