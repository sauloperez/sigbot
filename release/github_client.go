package release

import (
	"context"
	"fmt"
	"github.com/google/go-github/github"
	"golang.org/x/oauth2"
	"os"
	"time"
)

// Declares go-github's interface so we can provide fake implementations in tests.
type GithubAPIClient interface {
	CreateRelease(context.Context, string, string, *github.RepositoryRelease) (*github.RepositoryRelease, *github.Response, error)
	GetLatestRelease(context.Context, string, string) (*github.RepositoryRelease, *github.Response, error)
}

// Defines go-github's Search interface so we can provide fake implementations in tests.
type SearchAPI interface {
	Issues(context.Context, string, *github.SearchOptions) (*github.IssuesSearchResult, *github.Response, error)
}

// Wraps go-github in a public API of our own declouping Sigbot's services from
// go-github's design decisions. It provides methods that better suit our
// needs.
type GithubClient struct {
	owner     string
	repo      string
	APIClient GithubAPIClient
	Search    SearchAPI
}

func NewGithubClient(owner, repo string) *GithubClient {
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: os.Getenv("SIGBOT_PERSONAL_TOKEN")},
	)
	tc := oauth2.NewClient(ctx, ts)

	githubClient := github.NewClient(tc)

	return &GithubClient{
		owner:     owner,
		repo:      repo,
		APIClient: githubClient.Repositories,
		Search:    githubClient.Search,
	}
}

// Creates a new Github release in draft state
func (g *GithubClient) DraftRelease(r Release) (Release, error) {
	name := r.TagName + " " + r.CodeName
	draft := true

	// TODO: Add sigbot as a regular Github user an pass it in here
	repositoryRelease := &github.RepositoryRelease{
		Name:    &name,
		Draft:   &draft,
		TagName: &r.TagName,
		Body:    &r.Description,
	}

	release, _, err := g.APIClient.CreateRelease(
		context.Background(),
		g.owner,
		g.repo,
		repositoryRelease,
	)

	return Release{
		URL:         *release.HTMLURL,
		Description: *release.Body,
		TagName:     *release.TagName,
	}, err
}

// Returns the last release present in Github
func (g *GithubClient) GetLatestRelease() (Release, error) {
	r, _, err := g.APIClient.GetLatestRelease(context.Background(), g.owner, g.repo)
	release := Release{}

	if err == nil {
		// TODO: Missing CodeName from TagName + Name
		release = Release{
			URL:         *r.HTMLURL,
			TagName:     *r.TagName,
			Description: *r.Body,
			PublishedAt: *r.PublishedAt,
			Committish:  *r.TargetCommitish,
		}
	}

	return release, err
}

type PullRequest struct {
	Number int
	URL    string
	Title  string
}

func (g *GithubClient) GetPullRequestsSince(date time.Time) ([]PullRequest, error) {
	d := date.Format("2006-01-02T15:04:05-0700")
	query := fmt.Sprintf("is:pr repo:%s/%s merged:>%s", g.owner, g.repo, d)

	result, _, err := g.Search.Issues(context.Background(), query, nil)

	var pullRequests []PullRequest

	if err != nil {
		return pullRequests, err
	}

	for _, issue := range result.Issues {
		pullRequests = append(
			pullRequests,
			PullRequest{*issue.Number, *issue.HTMLURL, *issue.Title},
		)
	}

	return pullRequests, nil
}
