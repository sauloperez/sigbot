# Sigbot

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/sauloperez/sigbot)](https://goreportcard.com/report/gitlab.com/sauloperez/sigbot)

Open Food Network's Norwegian army knife

## Usage

```
sigbot release new --codename Potato # Drafts the next minor release version
sigbot build get v1.30.0
```

### Config

There are a number of settings you can configure by means of environment variables, perfectly suited for playing with the tool without bothering team mates. These are:

* SIGBOT_OWNER: the Github organization that Sigbot will interact with. You can use this to point to an organization other than `openfoodfoundation`.
* SIGBOT_REPO: the Github repository owner that Sigbot will interact with. You use this to point to a repo other than `openfoodnetwork`.
* SIGBOT_PERSONAL_TOKEN: the API token used to authenticate against Github. You can generate one from your Github account settings.
* SIGBOT_PROJECT_ID: the Semaphore CI's project that Sigbot will work with. Like Github's config settings, you can point to a different project if you need to.

#### TODO

* SIGBOT_SEMAPHORE_BRANCH_ID
* SIGBOT_SEMAPHORE_TOKEN

### release new

Creates a new draft release in Github with a list of all the pull requests that got merged since the last one; that is the changes release will bring.

It also infers the version number by incrementing the minor version following semantinc versioning. If you wish to use a different one, for a patch release for instance, you can provide the number using the `--version` argument.

The `--codename` argument is optional but recommended. This is the name the release will receive and it helps our communities refer to a specific release, which is easier and nicer than using a version number. The value's argument will be appended to the version to form the release header.

If no `--commitish` argument is provided, the HEAD of the current branch will be picked. So, if your current branch is `2-0-1` the release will point to the HEAD of its remote counterpart.

## TODO

```
sigbot releases # Needs some fixing
sigbot stage build --version 1.30.0
sigbot publish release --version 1.30.0 # It could announce it as well
sigbot announce release --version 1.30.0
```
