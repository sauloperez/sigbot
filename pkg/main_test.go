package main

import "testing"

func TestNextVersionWithoutV(t *testing.T) {
	expected := "1.0.1"
	actual := NextPatchVersion("1.0.0")
	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

func TestNextVersionWithV(t *testing.T) {
	expected := "v1.30.1"
	actual := NextPatchVersion("v1.30.0")
	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}
