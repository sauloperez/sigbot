package main

import (
	"context"
	"fmt"
	"strings"

	"github.com/blang/semver"
	"github.com/google/go-github/github"
)

func NextPatchVersion(currentVersion string) string {
	number := strings.TrimPrefix(currentVersion, "v")

	version, _ := semver.Make(number)
	nextNumber := fmt.Sprintf("%d.%d.%d", version.Major, version.Minor, version.Patch+1)

	nextVersion := ""
	if number == currentVersion {
		nextVersion = fmt.Sprintf("%v", nextNumber)
	} else {
		nextVersion = fmt.Sprintf("v%v", nextNumber)
	}

	return nextVersion
}

func main() {
	client := github.NewClient(nil)

	release, _, _ := client.Repositories.GetLatestRelease(context.Background(), "openfoodfoundation", "openfoodnetwork")
	currentVersion := *release.TagName

	fmt.Printf("Current version: %v\n", currentVersion)

	nextVersion := NextPatchVersion(currentVersion)

	fmt.Printf("Next version: %v\n", nextVersion)
}
