package releases

import (
	"github.com/google/go-github/github"
	"testing"
)

type FakeRepositories struct{}

func (_ FakeRepositories) ListReleases(_ *github.Client) []*github.RepositoryRelease {
	nyamera := "v1.30.0 Nyàmera"
	durian := "v1.29.0 Durian"

	return []*github.RepositoryRelease{&github.RepositoryRelease{Name: &nyamera}, &github.RepositoryRelease{Name: &durian}}
}

func TestHelloWorld(t *testing.T) {
	expected := []string{"v1.30.0 Nyàmera", "v1.29.0 Durian"}

	sigbot := Sigbot{}
	actual := sigbot.ListReleases(FakeRepositories{})

	for i, actualRelease := range actual {
		expectedRelease := expected[i]

		if *actualRelease != expectedRelease {
			t.Errorf("Test failed, expected: '%s', got: '%s'", expectedRelease, *actualRelease)
		}
	}

}
