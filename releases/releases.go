package releases

import (
	"context"
	"github.com/google/go-github/github"
)

type Sigbot struct {
	Client *github.Client
}

type RepositoriesAdapter interface {
	ListReleases(*github.Client) []*github.RepositoryRelease
}

type GithubRepositories struct{}

func (_ GithubRepositories) ListReleases(client *github.Client) []*github.RepositoryRelease {
	releases, _, _ := client.Repositories.ListReleases(context.Background(), "openfoodfoundation", "openfoodnetwork", nil)

	return releases
}

func (s Sigbot) ListReleases(adapter RepositoriesAdapter) []*string {
	releases := adapter.ListReleases(s.Client)

	var releaseNames []*string
	for _, release := range releases {
		releaseNames = append(releaseNames, release.Name)
	}

	return releaseNames
}
