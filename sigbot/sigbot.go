package sigbot

import (
	"gitlab.com/sauloperez/sigbot/build"
	"gitlab.com/sauloperez/sigbot/release"
)

// Sigbot encapsulates all the services the tool provides
type Sigbot struct {
	Build   *build.SemaphoreBuildService
	Release *release.GithubReleaseService
}

// Returns an initialized sigbot type
func NewSigbot() *Sigbot {
	return &Sigbot{
		Build:   build.NewSemaphoreBuildService(),
		Release: release.NewGithubReleaseService(),
	}
}
