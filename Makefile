PROJECT_NAME := "sigbot"
PKG := "gitlab.com/sauloperez/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/...)
GO_FILES := $(shell find . -name '*.go' | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint:
	@golint -set_exit_status ${PKG_LIST}

test:
	@go test -short ${PKG_LIST}

race: dep
	@go test -race -short ${PKG_LIST}

coverage:
	./scripts/coverage.sh;

coverhtml:
	./scripts/coverage.sh html;

dep:
	@go get -v -d ./...
	@go get -u github.com/golang/lint/golint

build: dep
	@go build -i -v $(PKG)

clean:
	@rm -f $(PROJECT_NAME)
