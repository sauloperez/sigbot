package build

import "github.com/ldez/go-semaphoreci/v1"

// Declares go-semaphoreci's interface so we can provide fake implementations in tests.
type SemaphoreAPIClient interface {
	Launch(string, int, string) (*v1.BuildInformation, error)
}

// Wraps go-semaphoreci in a public API of our own declouping Sigbot's services from go-semaphoreci's design decisions. It provides methods that better suit our needs.
type SemaphoreClient struct {
	APIClient SemaphoreAPIClient
	projectID string
	branchID  int
}

func NewSemaphoreClient(projectID string, branchID int, Token string) *SemaphoreClient {
	transport := v1.TokenTransport{Token: Token}

	APIclient := v1.NewClient(transport.Client()).Builds

	return &SemaphoreClient{
		APIclient,
		projectID,
		branchID,
	}
}

func (s *SemaphoreClient) Build(committish string) (Build, error) {
	buildInformation, err := s.APIClient.Launch(s.projectID, s.branchID, committish)
	if err != nil {
		return Build{}, err
	}

	return Build{URL: buildInformation.HTMLURL}, err
}
