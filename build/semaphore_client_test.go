package build

import (
	"errors"
	"github.com/ldez/go-semaphoreci/v1"
	"testing"
)

type FakeAPIClient struct {
	actualProjectID string
	actualBranchID  int
	actualCommitSHA string
}

func (f *FakeAPIClient) Launch(projectID string, branchID int, commitSHA string) (*v1.BuildInformation, error) {
	f.actualProjectID = projectID
	f.actualBranchID = branchID
	f.actualCommitSHA = commitSHA

	buildInformation := v1.BuildInformation{HTMLURL: "https://semaphoreci.com/openfoodfoundation/openfoodnetwork-2/branches/master/builds/562"}
	return &buildInformation, nil
}

func TestBuild(t *testing.T) {
	expected := Build{URL: "https://semaphoreci.com/openfoodfoundation/openfoodnetwork-2/branches/master/builds/562"}

	semaphoreClient := &SemaphoreClient{
		APIClient: &FakeAPIClient{},
		projectID: "projectId",
		branchID:  1,
	}
	actual, _ := semaphoreClient.Build("213")

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}

func TestBuildLaunchArguments(t *testing.T) {
	expectedBranchID := 1
	expectedCommitSHA := "commitsha"
	expectedProjectID := "projectId"

	APIClient := &FakeAPIClient{}
	semaphoreClient := &SemaphoreClient{
		APIClient: APIClient,
		projectID: "projectId",
		branchID:  1,
	}
	semaphoreClient.Build("commitsha")

	if APIClient.actualBranchID != expectedBranchID {
		t.Errorf("Test failed, expected: '%v', got: '%v'", expectedBranchID, APIClient.actualBranchID)
	}
	if APIClient.actualProjectID != expectedProjectID {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expectedProjectID, APIClient.actualProjectID)
	}
	if APIClient.actualCommitSHA != expectedCommitSHA {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expectedCommitSHA, APIClient.actualCommitSHA)
	}
}

type ErroredAPIClient struct{}

func (f *ErroredAPIClient) Launch(projectID string, branchID int, commitSHA string) (*v1.BuildInformation, error) {
	return nil, errors.New("Fake error message")
}

func TestBuildLaunchError(t *testing.T) {
	APIClient := &ErroredAPIClient{}
	semaphoreClient := &SemaphoreClient{
		APIClient: APIClient,
		projectID: "projectId",
		branchID:  1,
	}

	expected := "Fake error message"
	_, err := semaphoreClient.Build("213ga8")
	actual := err.Error()

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}
