package build

import "os"

type Build struct {
	URL string
}

type BuildService interface {
	GetBuild(string) (string, error)
}

type CIClient interface {
	Build(string) (Build, error)
}

type SemaphoreBuildService struct {
	Client CIClient
}

func (s *SemaphoreBuildService) GetBuild(releaseVersion string) (string, error) {
	build, err := s.Client.Build(releaseVersion)
	return build.URL, err
}

// TODO: make users store this personal token in a Sigbot's config file
// See: https://github.com/spf13/cobra#bind-flags-with-config
func NewSemaphoreBuildService() *SemaphoreBuildService {
	// FIXME: This clashes with the token used by the github client. Semaphore
	// must definitely need a token of its own.
	personalToken := os.Getenv("SIGBOT_PERSONAL_TOKEN")
	projectID := os.Getenv("SIGBOT_PROJECT_ID")

	// TODO: Read branchID from env and cast to int
	branchID := 1830903

	if personalToken == "" {
		personalToken = "YNc5tmktqng_EbUXPxbc"
	}
	if projectID == "" {
		projectID = "319c30b0-2076-47c9-95cd-019702e40d4e"
	}

	semaphoreClient := NewSemaphoreClient(projectID, branchID, personalToken)
	return &SemaphoreBuildService{Client: semaphoreClient}
}
