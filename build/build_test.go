package build

import "testing"

func TestGetBuild(t *testing.T) {
	expected := "https://semaphoreci.com/openfoodfoundation/openfoodnetwork-2/branches/master/builds/562"

	client := &SemaphoreClient{APIClient: &FakeAPIClient{}}
	semaphoreBuildService := SemaphoreBuildService{Client: client}
	actual, _ := semaphoreBuildService.GetBuild("v1.29.0")

	if actual != expected {
		t.Errorf("Test failed, expected: '%s', got: '%s'", expected, actual)
	}
}
