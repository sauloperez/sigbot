// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/sauloperez/sigbot/release"
	"gitlab.com/sauloperez/sigbot/sigbot"
)

// releaseCmd represents the release command
var releaseCmd = &cobra.Command{
	Use:   "release",
	Short: "Manage a release",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("release called")
	},
}

// newCmd builds a new draft release
var newCmd = &cobra.Command{
	Use:   "new",
	Short: "drafts a release",
	Run: func(cmd *cobra.Command, args []string) {
		codename, _ := cmd.Flags().GetString("codename")
		version, _ := cmd.Flags().GetString("version")
		commitish, _ := cmd.Flags().GetString("commitish")

		r := release.Release{
			CodeName:   codename,
			TagName:    version,
			Committish: commitish,
		}

		s := sigbot.NewSigbot()
		release, err := s.Release.CreateRelease(r)

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		fmt.Printf("%s done! You can check it out at %s\n", release.TagName, release.URL)
	},
}

func init() {
	newCmd.Flags().StringP("codename", "c", "", "Name of the release; i.e. Artichoke (optional)")
	newCmd.Flags().StringP("version", "v", "", "Number that will be used as git tag and release name; i.e. v2.0.1 (optional)")
	newCmd.Flags().StringP("commitish", "r", "", "Git reference such as commit or tag to point the release to; i.e. a4126c2 (optional)")

	releaseCmd.AddCommand(newCmd)

	rootCmd.AddCommand(releaseCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// releaseCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// releaseCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
